"""A word-counting workflow."""

# pytype: skip-file

import argparse
import logging
import re
import pyarrow

import apache_beam as beam
from apache_beam.io import ReadFromText
from apache_beam.options.pipeline_options import PipelineOptions
from apache_beam.options.pipeline_options import SetupOptions

def run(argv=None, save_main_session=True):
    """Main entry point; defines and runs the wordcount pipeline."""
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '--input',
        dest='input',
        help='Input file to process.')
    parser.add_argument(
        '--output',
        dest='output',
        required=True,
        help='Output file to write results to.')
    parser.add_argument('--filter_pattern', default="^.*FFFF.*$")

    known_args, pipeline_args = parser.parse_known_args(argv)

    # We use the save_main_session option because one or more DoFn's in this
    # workflow rely on global context (e.g., a module imported at module level).
    pipeline_options = PipelineOptions(pipeline_args)
    pipeline_options.view_as(SetupOptions).save_main_session = save_main_session

    # The pipeline will be run on exiting the with block.
    with beam.Pipeline(options=pipeline_options) as p:

        # Read the text file[pattern] into a PCollection.
        lines = p | 'Read' >> ReadFromText(known_args.input)

        counts = (
            lines
            | 'Parse' >> beam.Map(lambda x: beam.Row(col1=x[0:10], col2=x[12:44], col3=x[46:98]))
            | 'Filter' >> beam.Filter(lambda x: re.match(known_args.filter_pattern, x.col3) is not None)
            | 'PairWIthOne' >> beam.Map(lambda x: (x.col1, 1))
            | 'GroupAndCount' >> beam.combiners.Count.PerKey())

        # Format the counts into a PCollection of strings.
        def format_result(key, count):
            return {"col1":key, "count":count}

        output = counts | 'Format' >> beam.MapTuple(format_result)

        output | 'Write' >> beam.io.WriteToParquet(known_args.output,
            pyarrow.schema(
                [('col1', pyarrow.string()), ('count', pyarrow.int32())]
            ))


if __name__ == '__main__':
    logging.getLogger().setLevel(logging.INFO)
    run()
