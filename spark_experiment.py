from pyspark import sql
from pyspark.sql import functions as f

import argparse
import logging
import timeit

def parse_args():
    """Parse argv."""

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--input', required=True, help='GCS input location')
    parser.add_argument('--output', required=True, help='GCS output location')
    parser.add_argument('--filter_pattern', default="^.*FFFF.*$")

    return parser.parse_args()


def main(args):
    spark = sql.SparkSession.builder.appName('Spark SQL TeraSort').getOrCreate()
    sc = spark.sparkContext

    # 1. Read
    df = spark.read.text(args.input)

    # 2. Parse
    df = df.select(f.substring(f.col("value"), 1, 10).alias("col1"), f.substring(f.col("value"), 13, 32).alias("col2"), f.substring(f.col("value"), 47, 52).alias("col3"))

    # 3. Filter on a pattern
    df = df.where(f.col("col3").rlike(args.filter_pattern))

    # 4. Group by, count
    df = df.groupBy("col1").count() 
    
    # Write
    df.write.parquet(args.output)

    # df.orderBy('key').write.save(args.output)


if __name__ == '__main__':
    main(parse_args())
